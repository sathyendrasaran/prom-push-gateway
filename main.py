from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
import time

registry = CollectorRegistry()
g = Gauge('py_job_last_success_unixtime', 'Last time a batch job successfully finished', registry=registry)
while True:
    try:
        g.set_to_current_time()
        print("setting gauge metrics")
        print(g)
        push_to_gateway('http://prometheus-pushgateway.monitoring.svc:9091', job='PythonPushMetrics', registry=registry)
        print("Pushed to gateway")
        time.sleep(10)
    except Exception as e:
        print(e)
